#include <Adafruit_NeoPixel.h>

#define PIN 7

Adafruit_NeoPixel strip = Adafruit_NeoPixel(24, PIN, NEO_GRB + NEO_KHZ800);

int potpin = 0;
int potval = 0;

void setup() {
 
 Serial.begin(9600);

 strip.begin();
 strip.show(); // Initialize all pixels to 'off'
}

void loop() {
 potval = analogRead(potpin);
   Serial.println(potval);

 if ( potval <=40 ) {

//red_half
strip.setPixelColor(0, 255, 0, 0);//0
strip.setPixelColor(1, 0, 0, 0);//1
strip.setPixelColor(2, 255, 0, 0);//2
strip.setPixelColor(3, 0, 0, 0);//3
strip.setPixelColor(4, 255, 0, 0);//4
strip.setPixelColor(5, 0, 0, 0);//5
strip.setPixelColor(6, 255, 0, 0);//6
strip.setPixelColor(7, 0, 0, 0);//7
strip.setPixelColor(8, 255, 0, 0);//8
strip.setPixelColor(9, 0, 0, 0);//9
strip.setPixelColor(10, 255, 0, 0);//10
strip.setPixelColor(11, 0, 0, 0);//11
strip.setPixelColor(12, 255, 0, 0);//12
strip.setPixelColor(13, 0, 0, 0);//13
strip.setPixelColor(14, 255, 0, 0);//14
strip.setPixelColor(15, 0, 0, 0);//15
strip.setPixelColor(16, 255, 0, 0);//16
strip.setPixelColor(17, 0, 0, 0);//17
strip.setPixelColor(18, 255, 0, 0);//18
strip.setPixelColor(19, 0, 0, 0);//19
strip.setPixelColor(20, 255, 0, 0);//20
strip.setPixelColor(21, 0, 0, 0);//21
strip.setPixelColor(22, 255, 0, 0);//22
strip.setPixelColor(23, 0, 0, 0);//23

strip.show();

}    

else if ( potval <= 80){
//red_full
strip.setPixelColor(0, 255, 0, 0);//0
strip.setPixelColor(1, 255, 0, 0);//1
strip.setPixelColor(2, 255, 0, 0);//2
strip.setPixelColor(3, 255, 0, 0);//3
strip.setPixelColor(4, 255, 0, 0);//4
strip.setPixelColor(5, 255, 0, 0);//5
strip.setPixelColor(6, 255, 0, 0);//6

 
strip.show();

}

else if ( potval <= 120) {
//blue_half  
strip.setPixelColor(0, 0, 0,255 );//0
strip.setPixelColor(1, 0, 0, 0);//1
strip.setPixelColor(2, 0, 0, 255);//2
strip.setPixelColor(3, 0, 0, 0);//3
strip.setPixelColor(4, 0, 0, 255);//4
strip.setPixelColor(5, 0, 0, 0);//5
strip.setPixelColor(6, 0, 0, 255);//6
strip.setPixelColor(7, 0, 0, 0);//7
strip.setPixelColor(8, 0, 0, 255);//8


strip.show();

}

else if ( potval <= 160)
{
//blue_full    
strip.setPixelColor(0, 0, 0,255 );//0
strip.setPixelColor(1, 0, 0,255);//1
strip.setPixelColor(2, 0, 0, 255);//2
strip.setPixelColor(3,0, 0,255);//3
strip.setPixelColor(4, 0, 0, 255);//4
strip.setPixelColor(5,0, 0,255);//5
strip.setPixelColor(6, 0, 0, 255);//6
strip.setPixelColor(7, 0, 0,255);//7


strip.show();

}
 
  else if ( potval <= 200){
//green_half  
strip.setPixelColor(0, 0, 255, 0);//0
strip.setPixelColor(1, 0, 0, 0);//1
strip.setPixelColor(2, 0, 255, 0);//2
strip.setPixelColor(3, 0, 0, 0);//3
strip.setPixelColor(4, 0, 255, 0);//4
strip.setPixelColor(5, 0, 0, 0);//5
strip.setPixelColor(6, 0, 255, 0);//6

strip.show();

}

else if ( potval <= 240){
//green_full
strip.setPixelColor(0, 0, 255, 0);//0
strip.setPixelColor(1, 0, 255, 0);//1
strip.setPixelColor(2, 0, 255, 0);//2
strip.setPixelColor(3, 0, 255, 0);//3
strip.setPixelColor(4, 0, 255, 0);//4


strip.show();

}

else if ( potval <= 300)
{
//green/blue_half
strip.setPixelColor(0, 0, 255, 0);//0
strip.setPixelColor(1, 0, 0, 0);//1
strip.setPixelColor(2, 0, 0, 255);//2
strip.setPixelColor(3, 0, 0, 0);//3
strip.setPixelColor(4, 0, 255, 0);//4
strip.setPixelColor(5, 0, 0, 0);//5
strip.setPixelColor(6, 0, 0, 255);//6
strip.setPixelColor(7, 0, 0, 0);//7
strip.setPixelColor(8, 0, 255, 0);//8
strip.setPixelColor(9, 0, 0, 0);//9
strip.setPixelColor(10, 0, 0, 255);//10
strip.setPixelColor(11, 0, 0, 0);//11
strip.setPixelColor(12, 0, 255, 0);//12


strip.show();

}

else if ( potval <= 340){
//green/blue_full
strip.setPixelColor(0, 0, 255, 0);//0
strip.setPixelColor(1, 0,0, 255);//1
strip.setPixelColor(2, 0, 255, 0);//2
strip.setPixelColor(3, 0, 0, 255);//3


strip.show();

}

else if ( potval <= 380)
{
//green/red_half
strip.setPixelColor(0, 0, 255, 0);//0
strip.setPixelColor(1, 0, 0, 0);//1
strip.setPixelColor(2, 255, 0, 0);//2
strip.setPixelColor(3, 0, 0, 0);//3
strip.setPixelColor(4, 0, 255, 0);//4
strip.setPixelColor(5, 0, 0, 0);//5



strip.show();

}

else if ( potval <= 420){
//green/red_full
strip.setPixelColor(0, 0, 255, 0);//0
strip.setPixelColor(1, 255, 0, 0);//1
strip.setPixelColor(2, 0, 255, 0);//2
strip.setPixelColor(3, 255, 0, 0);//3


strip.show();

}

else if ( potval <= 460){
//green/magneta_half
strip.setPixelColor(0, 0, 255, 0);//0
strip.setPixelColor(1, 0, 0, 0);//1
strip.setPixelColor(2, 255, 0, 255);//2
strip.setPixelColor(3, 0, 0, 0);//3
strip.setPixelColor(4, 0, 255, 0);//4
strip.setPixelColor(5, 0, 0, 0);//5
strip.setPixelColor(6, 255,0, 255);//6
strip.setPixelColor(7, 0, 0, 0);//7


strip.show();

}

else if ( potval <= 500){
//green/magenta_full
strip.setPixelColor(0, 0, 255, 0);//0
strip.setPixelColor(1, 255, 0, 255);//1
strip.setPixelColor(2, 0, 255, 0);//2
strip.setPixelColor(3, 255, 0, 255);//3
strip.setPixelColor(4, 0, 255, 0);//4
strip.setPixelColor(5, 255, 0, 255);//5
strip.setPixelColor(6, 0, 255, 0);//6
strip.setPixelColor(7, 255, 0, 255);//7
strip.setPixelColor(8, 0, 255, 0);//8
strip.setPixelColor(9, 255, 0, 255);//9
strip.setPixelColor(10, 0, 255, 0);//10
strip.setPixelColor(11, 255, 0, 255);//11
strip.setPixelColor(12, 0, 255, 0);//12
strip.setPixelColor(13, 255, 0, 255);//13
strip.setPixelColor(14, 0, 255, 0);//14
strip.setPixelColor(15, 255, 0, 255);//15
strip.setPixelColor(16, 0, 255, 0);//16
strip.setPixelColor(17, 255, 0, 255);//17
strip.setPixelColor(18, 0, 255, 0);//18
strip.setPixelColor(19, 255, 0, 255);//19
strip.setPixelColor(20, 0, 255, 0);//20
strip.setPixelColor(21, 255, 0, 255);//21
strip.setPixelColor(22, 0, 255, 0);//22
strip.setPixelColor(23, 255, 0, 255);//23

strip.show();

}

else if ( potval <= 540){
//green/magenta_quarter
strip.setPixelColor(0, 0, 255, 0);//0
strip.setPixelColor(1, 0, 0, 0);//1
strip.setPixelColor(2, 0, 0, 0);//2
strip.setPixelColor(3, 255, 0, 255);//3
strip.setPixelColor(4, 0, 0, 0);//4


strip.show();

}

else if ( potval <= 600){
//blue/magenta_half
strip.setPixelColor(0, 0, 0, 255);//0
strip.setPixelColor(1, 0, 0, 0);//1
strip.setPixelColor(2, 255, 0, 255);//2
strip.setPixelColor(3, 0, 0, 0);//3

 


strip.show();

}

else if ( potval <= 640){
//blue/magenta_full
strip.setPixelColor(0, 0, 0, 255);//0
strip.setPixelColor(1, 255, 0, 255);//1
strip.setPixelColor(2, 0, 0,255);//2
strip.setPixelColor(3, 255, 0, 255);//3
strip.setPixelColor(4, 0, 0, 255);//4
strip.setPixelColor(5, 255, 0, 255);//5
strip.setPixelColor(6, 0, 0, 255);//6



strip.show();

}

else if ( potval <= 700){
//red/magenta_half
strip.setPixelColor(0, 255, 0, 0);//0
strip.setPixelColor(1, 0, 0, 0);//1
strip.setPixelColor(2, 255, 0, 255);//2
strip.setPixelColor(3, 0, 0, 0);//3
strip.setPixelColor(4, 255, 0, 0);//4
strip.setPixelColor(5, 0, 0, 0);//5
strip.setPixelColor(6, 255,0, 255);//6
strip.setPixelColor(7, 0, 0, 0);//7

 

strip.show();

}

else if ( potval >740){
//red/blue_half
strip.setPixelColor(0, 255, 0, 0);//0
strip.setPixelColor(1, 0, 0, 0);//1
strip.setPixelColor(2, 0, 255,0);//2
strip.setPixelColor(3, 0, 0, 0);//3
strip.setPixelColor(4, 0, 0, 255);//4
strip.setPixelColor(5, 0, 0, 0);//5


strip.show();

}

else if ( potval <= 780){
//red/green/blue_full
strip.setPixelColor(0, 255, 0, 0);
strip.setPixelColor(1, 0, 255, 0);
strip.setPixelColor(2, 0, 0,255);
strip.setPixelColor(3, 255, 0, 0);
strip.setPixelColor(4, 0, 255, 0);
strip.setPixelColor(5, 0, 0, 255);
strip.setPixelColor(6, 255, 0, 0);
strip.setPixelColor(7, 0, 255, 0);
strip.setPixelColor(8, 0, 0, 255);
strip.setPixelColor(9, 255, 0, 0);
strip.setPixelColor(10,0 , 255, 0);



strip.show();

}





}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
 uint16_t i, j;

 for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
   for(i=0; i< strip.numPixels(); i++) {
     strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255));
   }
   strip.show();
   delay(20);
 }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
 if(WheelPos < 85) {
  return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
 } else if(WheelPos < 170) {
  WheelPos -= 85;
  return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
 } else {
  WheelPos -= 170;
  return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
 }
}
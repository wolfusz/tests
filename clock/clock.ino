
int clockInterrupt = 0; 
int pwmOut = 6;

int seconds = 0;
int minutes = 0;
int hours = 0;
int masterClock = 0;

void setup() {
  attachInterrupt(clockInterrupt, clockCounter, RISING);
  delay(1000);
  Serial.begin(115200);
  analogReference(DEFAULT);
  analogWrite(pwmOut, 127);
  Serial.println("Setup done");
}

void loop() {
  // // print time
  // if (hours < 10) Serial.print("0");
  // Serial.print(hours);
  // Serial.print(":");
  // if (minutes < 10) Serial.print("0");
  // Serial.print(minutes);
  // Serial.print(":");
  // if (seconds < 10) Serial.print("0");
  // Serial.println(seconds);
  
  delay(100);
}

void clockCounter() {
  masterClock++;
  if (masterClock == 974) {
    seconds++;
    masterClock = 0;

    if (seconds == 60) {
      minutes++;
      seconds = 0;
    }
  
    if (minutes == 60) {
      hours++;
      minutes = 0;
    }
  }
  return;
}


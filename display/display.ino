#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>

// Software SPI (slower updates, more flexible pin options):
// pin 1 - Serial clock out (SCLK)
// pin 2 - Serial data out (DIN)
// pin 3 - Data/Command select (D/C)
// pin 4 - LCD chip select (CS)
// pin 5 - LCD reset (RST)
Adafruit_PCD8544 display = Adafruit_PCD8544(8, 9, 10, 12, 11);

void drawPixel(int x, int y) {
  display.drawPixel(84-x, 48-y, BLACK);
}

void setup() {
  Serial.begin(9600);
  
  display.begin();
  display.setContrast(60);
  
//#   splash screen
//  display.display();
//  delay(2000);
  display.clearDisplay();

  int x,y;
  for (x = 41; x < 44; x++) drawPixel( x, 2);
  for (x = 40; x < 45; x+=2) drawPixel( x, 3);
  drawPixel(40, 4); drawPixel(44, 4);
  for (x = 40; x < 45; x++) drawPixel( x, 5);
  
  for (y = 6; y < 14; y++) {
    drawPixel(40, y); drawPixel(44, y);
  }
    
  for (x = 37; x < 41; x++) drawPixel( x, 14);
  for (x = 44; x < 48; x++) drawPixel( x, 14);
  for (y = 15; y < 18; y++) {
    drawPixel(36, y); drawPixel(48, y);
  }
  drawPixel(42, 17);
  for (x = 37; x < 42; x++) drawPixel( x, 18);
  for (x = 43; x < 48; x++) drawPixel( x, 18);
  
    
  display.display();
}

void loop() {
  
}

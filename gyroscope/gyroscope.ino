#include<Wire.h>

#define PWR_MGMT_1       0x6B // Device defaults to the SLEEP mode
#define PWR_MGMT_2       0x6C

#define MPU6050_ADDRESS 0x68
float AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;

String command = "";
bool commandArrived = false;

void setup(){
  command.reserve(100);

  // setup gyroscope
  Wire.begin();
  Wire.beginTransmission(MPU6050_ADDRESS);
  Wire.write(PWR_MGMT_1);  // PWR_MGMT_1 register
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);
  
  Serial.begin(19200);
  Serial.println("Setup finished.");
}

/* MAIN LOOP */
void loop(){
  if (commandArrived)
    processCommand();
}

/* COMMAND HANDLING */
// read command
void serialEvent() {
  while (Serial.available()) {
    char in = (char)Serial.read();
    if (in == '$')
      commandArrived = true;
    else
      command += in;
  }
}

// process command
void processCommand() {
  // connecting with desktop application
  if (command == F("W2789"))
    Serial.print(F("wlf$"));
  // sending gyroscope data
  else if (command == F("GY521")) {
    // get data
    getGyroscopeData();
    // send to serial
    Serial.print(AcX); Serial.print(F(","));
    Serial.print(AcY); Serial.print(F(","));
    Serial.print(AcZ); Serial.print(F(","));
    Serial.print(GyX/131); Serial.print(F(","));
    Serial.print(GyY/131); Serial.print(F(","));
    Serial.print(GyZ/131);
    Serial.print(F(","));
    Serial.print(Tmp/340.0+36.53);
    Serial.println();
  }
  // erase command info
  command = "";
  commandArrived = false;
}


/* MODULES HANDLING */
void getGyroscopeData() {
  // wake up module
  Wire.beginTransmission(MPU6050_ADDRESS);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU6050_ADDRESS,14,true);
  // read data
  AcX = Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)
  AcY = Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ = Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  AcX /= 16384;
  AcY /= 16384;
  AcZ /= 16384;
  Tmp = Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
  GyX = Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY = Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ = Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
}

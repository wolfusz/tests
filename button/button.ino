int buttonPin = 7;
boolean buttonDown = false;

void setup() {
  Serial.begin(9600);
  pinMode(buttonPin, INPUT);
  digitalWrite(buttonPin, HIGH);
}

void loop() {
  while ( digitalRead(buttonPin) == HIGH )
    buttonDown = true;

  if (buttonDown == true) {
    Serial.print("1");
    buttonDown = false;
    delay(100);
  }
}




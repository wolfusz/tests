#include <LCD.h>
#include <Button.h>

int buttonPin = 7;

void setup() {
  Serial.begin(9600);
  
  LCD::init(8, 9, 10, 12, 11, true);
  LCD::setContrast(60);
  
  pinMode(4, OUTPUT); // init led

  Button::init(buttonPin);
}


//byte bitmap[] = {0x0f,0x81,0x18,0x42,0xff};
void loop() {
  int b = Button::getState(buttonPin);
  if (b == press) {
    digitalWrite(4, HIGH);
  } else if(b == release) {
    digitalWrite(4, LOW);
  }
  
  LCD::clear();
  LCD::string(0,0,"TEST\0");
  LCD::update();
  
  delay(100);
}

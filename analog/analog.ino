int analogButton = 8;
int joyPin1 = 0;                 // slider variable connecetd to analog pin 0
int joyPin2 = 1;                 // slider variable connecetd to analog pin 1
int value1 = 0;                  // variable to read the value from the analog pin 0
int value2 = 0;                  // variable to read the value from the analog pin 1

void setup() {
  Serial.begin(9600);
  pinMode(analogButton,INPUT);
  digitalWrite(analogButton,HIGH);
}

int treatValue(int data) {
  return (data * 9 / 1024) + 48;
}

void loop() {
  value1 = analogRead(joyPin1);
  delay(100);
  value2 = analogRead(joyPin2);

  int buttonState = digitalRead(analogButton);
  char butt = '0';
  if (buttonState == LOW)
    butt = '1';
    
  Serial.print(butt);
  Serial.print("     ");
  Serial.print(treatValue(value1));
  Serial.print("   ");
  Serial.print(treatValue(value2));
  Serial.print((char)10);
  Serial.print((char)13);
}
